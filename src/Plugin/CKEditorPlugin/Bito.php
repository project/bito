<?php

/**
 * @file
 * Contains \Drupal\bito\Plugin\CKEditorPlugin\Bito.
 */

namespace Drupal\bito\Plugin\CKEditorPlugin;

use Drupal\bito\Form\BitoConfigForm;
use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the "bito" plugin.
 *
 * @CKEditorPlugin(
 *   id = "bito",
 *   label = @Translation("Bito"),
 *   module = "bito"
 * )
 */
class Bito extends CKEditorPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The Bito configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $configFactory->get(BitoConfigForm::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return drupal_get_path('module', 'bito') . '/assets/js/plugins/bito/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'Bito' => [
        'label' => $this->t('Bito'),
        'image' => drupal_get_path('module', 'bito') . '/assets/js/plugins/bito/bito.png',
      ],
    ];
  }

}
