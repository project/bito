<?php

/**
 * @file
 * Contains \Drupal\bito\Form\BitoEditorDialog.
 */

namespace Drupal\bito\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\filter\Entity\FilterFormat;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a bito dialog for text editors.
 */
class BitoEditorDialog extends FormBase {


  /**
   * The Bito configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The editor storage service.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $editorStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityStorageInterface $editor_storage, ConfigFactoryInterface $configFactory) {
    $this->editorStorage = $editor_storage;
    $this->config = $configFactory->get(BitoConfigForm::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')->getStorage('editor'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bito_editor_dialog_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, FilterFormat $filter_format = NULL) {
    $user_input = $form_state->getUserInput();
    $input = isset($user_input['editor_object']) ? $user_input['editor_object'] : [];

    $form['#tree'] = TRUE;
    $form['#attached']['library'][] = 'editor/drupal.editor.dialog';
    $form['#prefix'] = '<div id="bito-editor-dialog-form">';
    $form['#suffix'] = '</div>';

    if (empty($this->config->get('accessKey'))) {
      $form['attributes']['info'] = [
        '#type' => 'inline_template',
        '#template' => '<span style="color: red">Your Project Access key must be set on: </span><a href="{{ href }}" target="_blank">Bito Configuration</a> page',
        '#context' => [
          'href' => '/admin/config/services/bito',
        ],
      ];
    }

    $bitoId = isset($input['bitoId']) ? $input['bitoId'] : '';
    $bitoId = empty($bitoId) && isset($input['data-id']) ? $input['data-id'] : '';

    $form['attributes']['bitoId'] = [
      '#type' => 'textfield',
      '#title' => 'Bito ID',
      '#description' => $this->t('The ID can be retrieved on Bito edit page on the platform'),
      '#required' => TRUE,
      '#default_value' => $bitoId,
    ];

    $bitoAdminLabel = isset($input['adminLabel']) ? $input['adminLabel'] : '';
    $bitoAdminLabel = empty($bitoAdminLabel) && isset($input['data-admin-label']) ? $input['data-admin-label'] : '';

    $form['attributes']['adminLabel'] = [
      '#type' => 'textfield',
      '#title' => 'Admin label',
      '#description' => $this->t('The label to help identify this Bito in wysiwyg view'),
      '#required' => FALSE,
      '#default_value' => $bitoAdminLabel,
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['save_modal'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#submit' => [],
      '#ajax' => [
        'callback' => '::submitForm',
        'event' => 'click',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $attributes = array_filter($form_state->getValue('attributes'));
    $form_state->setValue('attributes', $attributes);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();

    if ($form_state->getErrors()) {
      unset($form['#prefix'], $form['#suffix']);
      $form['status_messages'] = [
        '#type' => 'status_messages',
        '#weight' => -10,
      ];
      $response->addCommand(new HtmlCommand('#bito-editor-dialog-form', $form));
    }
    else {
      $response->addCommand(new EditorDialogSave($form_state->getValues()));
      $response->addCommand(new CloseModalDialogCommand());
    }

    return $response;
  }

}
