<?php

namespace Drupal\bito\Plugin\Filter;

use Drupal\bito\Form\BitoConfigForm;
use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a filter to process Bito placeholders.
 *
 * @Filter(
 *   id = "filter_bito",
 *   title = @Translation("Process Bito elements"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE
 * )
 */
class Bito extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The Bito configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $configFactory->get(BitoConfigForm::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {

    $result = new FilterProcessResult($text);
    if (stristr($text, 'bito-content') === FALSE) {
      return $result;
    }

    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);

    foreach ($xpath->query('//div[@data-admin-label]') as $node) {
      // Remove administrative data.
      $node->removeAttribute('data-admin-label');
      // Hidden initially and removed after successfully fetching.
      $node->setAttribute('style', 'display=none;');
      // Clean-up.
      $node->setAttribute('data-id', trim($node->getAttribute('data-id')));
    }

    $result->setProcessedText(Html::serialize($dom));

    // Add needed JS.
    $result->addAttachments([
      'library' => ['bito/fetch'],
      'drupalSettings' => ['bito' => ['accessKey' => $this->config->get('accessKey')]],
    ]);

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function tips($long = FALSE) {
    return $this->t('Fetch Bito content');
  }

}
