<?php

namespace Drupal\bito\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class BitoConfigForm.
 */
class BitoConfigForm extends ConfigFormBase {

  /**
   * This config name.
   */
  const CONFIG_NAME = 'bito.config';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bito_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(self::CONFIG_NAME);

    $form['info'] = [
      '#markup' => $this->t('The Access Key can be retrieved on the Bito platform by accessing the "Credentials" tab on the Project page'),
    ];

    $form['accessKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Key'),
      '#default_value' => $config->get('accessKey') ?? '',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config(self::CONFIG_NAME)
      ->set('accessKey', trim($form_state->getValue('accessKey')))->save();
  }

}
