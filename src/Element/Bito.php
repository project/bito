<?php

namespace Drupal\bito\Element;

use Drupal\Core\Render\Element\FormElement;
use Drupal\bito\Form\BitoConfigForm;

/**
 * Provides a bit form element.
 *
 * Usage example:
 *
 * @code
 * $form['banner'] = [
 *   '#type' => 'bito',
 *   '#id' => '9d3nilDiKHfDAYCMYkx1brF7nokNn1Yryy26hRS66p2eCtarGCSvVw7bVRdG',
 *   '#placeholders' => [
 *     'time' => time(),
 *   ],
 * ];
 * @endcode
 *
 * @FormElement("bito")
 */
class Bito extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#id' => '',
      '#placeholders' => [],
      '#pre_render' => [
        [$class, 'preRenderBito'],
      ],
    ];
  }

  /**
   * Pre-render bit callback.
   *
   * {@inheritdoc}
   */
  public static function preRenderBito($element) {

    $bitoId = $element['#id'];
    if (empty($bitoId)) {
      return [];
    };

    if (!$accessKey = \Drupal::configFactory()
      ->get(BitoConfigForm::CONFIG_NAME)
      ->get('accessKey')) {
      \Drupal::logger('bito')->warning('Bito Project access key missing');
      return [];
    }

    $element['#theme'] = 'bito';
    $element['#id'] = $bitoId;
    $element['#attached']['library'][] = 'bito/fetch';
    $element['#attached']['drupalSettings']['bito']['accessKey'] = $accessKey;

    if (!empty($element['#placeholders'])) {
      $element['#placeholders'] = json_encode($element['#placeholders'], JSON_HEX_APOS|JSON_FORCE_OBJECT);
    }

    return $element;
  }

}
