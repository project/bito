<?php

namespace Drupal\bito\Plugin\Block;

use Drupal\bito\Form\BitoConfigForm;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block rendering a remote Bito content.
 *
 * @Block(
 *   id = "bito_block",
 *   admin_label = @Translation("Bito"),
 * )
 */
class BitoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The Bito configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $configFactory->get(BitoConfigForm::CONFIG_NAME);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@block}
   */
  public function build() {
    $config = $this->getConfiguration();
    return [
      '#type' => 'bito',
      '#id' => $config['bitoId'],
    ];
  }

  /**
   * {@block}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    if (empty($this->config->get('accessKey'))) {
      $form['info'] = [
        '#type' => 'inline_template',
        '#template' => '<span style="color: red">Your Project Access Key is missing. Please add it on: </span><a href="{{ href }}" target="_blank">Bito Configuration</a> page',
        '#context' => [
          'href' => '/admin/config/services/bito',
        ],
      ];
    }

    $form['bitoId'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bito ID'),
      '#description' => $this->t('The ID can be retrieved on Bito edit page'),
      '#required' => TRUE,
      '#default_value' => $this->getConfiguration()['bitoId'] ?? '',
    ];

    return $form;
  }

  /**
   * {@block}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration = ['bitoId' => trim($form_state->getValue('bitoId'))];
  }

}
