BITO
----

### About

This module integrates [Bito](https://bito.host) service into Drupal.

The term "Bito", is used to refer to a remote piece of content.

See [Bito Docs](https://bito.host/docs/drupal) for more info

### Features

* Remote content management
* Content Scheduling

### Installation

```
composer require drupal/bito
```

### Configuration & Usage

* Grab your Project's Access Key on the Bito Platform and add it on Admin > Structure > Web services > Bito (
  /admin/config/services/bito).

* To place a Bito content on your site you have three ways:
    * Through UI - A "Bito" block is available which can be placed through Admin > Structure > Blocks (
      /admin/structure/block) page

    * Through UI - A "Bito" CKEditor plugin is available which allows placing your remote content in a wysiwyg enabled
      field. To enable it, on the text format edit page, drag the Bito(circle icon) from "Available buttons" to "Active
      toolbar" and enable the "Process Bito elements" filter.

    * Through Code - A "bito" element type is available which can be placed anywhere in your code, and it will be
      rendered as a rendarable array
      ```php
      $form['banner'] = [
        '#type' => 'bito',
        '#id' => '9d3nilDiKHfDAYCMYkx1brF7nokNn1Yryy26hRS66p2eCtarGCSvVw7bVRdG',
      ];
      ```

### Placeholders

Placeholders can be passed when using the "bito" element type programmatically e.g:
```php
$form['banner'] = [
  '#type' => 'bito',
  '#id' => '9d3nilDiKHfDAYCMYkx1brF7nokNn1Yryy26hRS66p2eCtarGCSvVw7bVRdG',
  '#placeholders' => [
    'time' => my_dynamic_data(),
  ],
];
```

For this to work, on Bito platform place `${time}` as a string in content area, so it is replaced accordingly.

### Events

When the Bito content is fetched:
- If fetched successfully, a JavaScript event `bitoFetchSuccess`. Listen to the event example below:

```javascript
JQuery(".bito.bito-content").on('bitoFetchSuccess', function (event, id, height, width) {
    // My code.
});
```

- If fetching failed, a JavaScript event `bitoFetchFailed`. Listen to the event example below:

```javascript
JQuery(".bito.bito-content").on('bitoFetchFailed', function (event, id) {
    // My code.
});
```
