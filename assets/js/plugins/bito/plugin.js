/**
 * @file
 * Bito plugin.
 *
 * @ignore
 */

(function ($, Drupal, drupalSettings, CKEDITOR) {

  'use strict';

  CKEDITOR.plugins.add('bito', {
    init: function (editor) {
      editor.addCommand('bito', {
        allowedContent: new CKEDITOR.style({
          element: 'div',
          attributes: {
            'class': '',
            'data-id': '',
            'data-admin-label': '',
          }
        }),
        requiredContent: new CKEDITOR.style({
          element: 'div',
          attributes: {
            'class': '',
            'data-id': '',
          }
        }),
        modes: {wysiwyg: 1},
        canUndo: true,
        exec: function (editor) {
          var bitoElement = getSelectedBito(editor);
          var bitoDOMElement = null;

          // Set existing values based on selected element.
          var existingValues = {};
          if (bitoElement && bitoElement.$) {
            bitoDOMElement = bitoElement.$;

            // Populate an array with the element's current attributes.
            var attribute = null;
            var attributeName;
            for (var attrIndex = 0; attrIndex < bitoDOMElement.attributes.length; attrIndex++) {
              attribute = bitoDOMElement.attributes.item(attrIndex);
              attributeName = attribute.nodeName.toLowerCase();
              // Don't consider data-cke-saved- attributes; they're just there
              // to work around browser quirks.
              if (attributeName.substring(0, 15) === 'data-cke-saved-') {
                continue;
              }
              // Store the value for this attribute, unless there's a
              // data-cke-saved- alternative for it, which will contain the
              // quirk-free, original value.
              existingValues[attributeName] = bitoElement.data('cke-saved-' + attributeName) || attribute.nodeValue;
            }
          }

          // Prepare a save callback to be used upon saving the dialog.
          var saveCallback = function (returnValues) {
            editor.fire('saveSnapshot');

            var finalAttributes = returnValues.attributes;
            finalAttributes['class'] = 'bito bito-content bito-wysiwyg';

            if (finalAttributes.bitoId) {
              finalAttributes['data-id'] = finalAttributes.bitoId;
              delete finalAttributes.bitoId;
            }

            if (typeof finalAttributes.adminLabel !== 'undefined') {
              finalAttributes['data-admin-label'] = finalAttributes.adminLabel;
              delete finalAttributes.adminLabel;
            }
            else{
              finalAttributes['data-admin-label'] = 'Bito ID: ' + finalAttributes['data-id'];
            }

            // Create a new Bito element if needed.
            if (!bitoElement && finalAttributes['data-id']) {
              var selection = editor.getSelection();
              // var range = selection.getRanges(1)[0];
              var range = selection.getRanges()[0];

              // Create the new div container by applying a style to the new text.
              var style = new CKEDITOR.style({element: 'div', attributes: finalAttributes});
              style.type = CKEDITOR.STYLE_INLINE;
              style.applyToRange(range);
              range.select();

              // Set the link so individual properties may be set below.
              bitoElement = getSelectedBito(editor);
            }
            // Update the element properties.
            else if (bitoElement) {
              for (var attrName in finalAttributes) {
                if (finalAttributes.hasOwnProperty(attrName)) {
                  // Update the property if a value is specified.
                  if (finalAttributes[attrName].length > 0) {
                    var value = finalAttributes[attrName];
                    bitoElement.data('cke-saved-' + attrName, value);
                    bitoElement.setAttribute(attrName, value);
                  }
                  // Delete the property if set to an empty string.
                  else {
                    bitoElement.removeAttribute(attrName);
                  }
                }
              }
            }

            // Save snapshot for undo support.
            editor.fire('saveSnapshot');
          };

          var dialogSettings = {dialogClass: 'editor-bito-dialog'};

          // Open the dialog for the edit form.
          Drupal.ckeditor.openDialog(editor, Drupal.url('bito/dialog/bito/' + editor.config.drupal.format), existingValues, saveCallback, dialogSettings);
        }
      });

      // Add buttons.
      if (editor.ui.addButton) {
        editor.ui.addButton('Bito', {
          label: 'Bito',
          command: 'bito',
          icon: this.path + '/bito.png'
        });
      }

      // Add CSS for edition state.
      var cssPath = this.path + 'bito.css';
      editor.on('mode', function () {
        if (editor.mode === 'wysiwyg') {
          this.document.appendStyleSheet(cssPath);
        }
      });

      // Editor mode.
      editor.on('doubleclick', function (evt) {
        var element = getSelectedBito(editor) || evt.data.element;


        if (!element.isReadOnly()) {
          if (element.is('div')) {
            editor.getSelection().selectElement(element);
            editor.getCommand('bito').exec();
          }
        }
      });
    }
  });

  /**
   * Get the surrounding bito element of current selection.
   * @param {CKEDITOR.editor} editor
   *   The CKEditor editor object
   *
   * @return {?HTMLElement}
   *   The selected element, or null.
   *
   */
  function getSelectedBito(editor) {
    var selection = editor.getSelection();
    var selectedElement = selection.getSelectedElement();

    if (selectedElement && selectedElement.is('div')) {
      return selectedElement;
    }

    var range = selection.getRanges(true)[0];

    if (range) {
      range.shrink(CKEDITOR.SHRINK_ELEMENT);
      return editor.elementPath(range.getCommonAncestor()).contains('div', 1);
    }
    return null;
  }

})(jQuery, Drupal, drupalSettings, CKEDITOR);
