(function ($, drupalSettings) {
  "use strict";

  Drupal.behaviors.Bito = {
    attach: function (context) {

      var accessKey = $.trim(((drupalSettings || {}).bito || {}).accessKey);
      $(".bito.bito-content").once("bito-populate").each(function () {
        var bitoEl = $(this), bitoId = $.trim(bitoEl.data("id"));
        var placeholdersObject = bitoEl.data("placeholders");

        $.ajax({
          async: true,
          type: "GET",
          url: "https://bito.host/api/v1/bit/" + bitoId,
          data: placeholdersObject ? {placeholders: placeholdersObject} : {},
          beforeSend: function( xhr ) {
            xhr.setRequestHeader('accessKey', accessKey);
          },
          success: function (data) {
            void 0 !== data.content && "" !== data.content && (bitoEl.html(bitoDecodeBase64(data.content)), bitoEl.removeAttr("data-id"), bitoEl.removeAttr("data-placeholders"), bitoEl.removeAttr("style"))
            bitoEl.trigger('bitoFetchSuccess', [bitoId, bitoEl.height(), bitoEl.width()]);
          },
          error: function () {
            bitoEl.trigger('bitoFetchFailed', [bitoId]);
          },
        })
      })

      function bitoDecodeBase64(str) {
        return decodeURIComponent(escape(window.atob(str)));
      }

    }
  }
}(jQuery, drupalSettings));
